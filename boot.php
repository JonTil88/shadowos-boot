#!ipxe

set menu-timeout 10000
isset ${menu-default} || set menu-default boot_stable
console --x 1000 --y 625
console --picture ${boot-url}/shadow_bg.png --left 30 --right 30 --top 30 --bottom 30
cpair --foreground 7 --background 15 2
cpair --foreground 0 --background 15 3

<?php if (isset($_GET['arch']) && $_GET['arch'] == "i386"):
  echo "echo BOOT : Hardware unsupported. This software is only compatible with 64 bit components.\n";
  echo "goto failed";
endif; ?>

:menu
  menu ShadowOS Boot Menu

  item --gap -- Choose ShadowOS version
  item --key 0 boot_stable (0) Boot from network on stable version of ShadowOS
  item --key 1 boot_stable_dis_nv (1) Boot from network on stable version of ShadowOS (Nouveau Disabled)
  item --key 2 boot_stable_dis_amd (2) Boot from network on stable version of ShadowOS (AMDGPU,Nouveau Disabled)
  item --key 3 boot_beta (3) Boot from network on beta version of ShadowOS
  item --key 4 boot_beta_dis_nv (4) Boot from network on beta version of ShadowOS (Nouveau Disabled)
  item --key 5 boot_alpha (5) Boot from network on alpha version of ShadowOS
  item --key 6 boot_alpha_dis_nv (6) Boot from network on alpha version of ShadowOS (Nouveau Disabled)
  item --gap -- Test tools
  item --key m test_memtest (M) Memory (Memtest)
  item --gap -- Advanced options
  item --key s shell (S)  Shell
  item --key r reboot (R)  Reboot computer
  item --key x shutdown (X) Shutdown
  choose --timeout ${menu-timeout} --default ${menu-default} selected || goto cancel
  set menu-timeout 0
  goto ${selected}

:boot_stable
  echo BOOT : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live/ quiet splash
  initrd http://91.121.38.1:8080/live/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/x86_64/archiso.img
  boot

:boot_stable_dis_nv
  echo BOOT ShadowOS stable : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live/ quiet splash splash nouveau.modeset=0
  initrd http://91.121.38.1:8080/live/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/x86_64/archiso.img
  boot
  
:boot_stable_dis_amd
  echo BOOT ShadowOS stable : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live/ quiet splash splash nouveau.modeset=0 radeon.si_support=0 radeon.cik_support=0 amdgpu.si_support=1 amdgpu.cik_support=1
  initrd http://91.121.38.1:8080/live/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live/arch/boot/x86_64/archiso.img
  boot

:boot_beta
  echo BOOT ShadowOS beta : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_beta/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_beta/ quiet splash
  initrd http://91.121.38.1:8080/live_beta/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live_beta/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live_beta/arch/boot/x86_64/archiso.img
  boot

:boot_beta_dis_nv
  echo BOOT ShadowOS beta : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_beta/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_beta/ quiet splash nouveau.modeset=0
  initrd http://91.121.38.1:8080/live_beta/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live_beta/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live_beta/arch/boot/x86_64/archiso.img
  boot
  
:boot_alpha
  echo BOOT ShadowOS beta : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_alpha/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_alpha/ quiet splash
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/x86_64/archiso.img
  boot

:boot_alpha_dis_nv
  echo BOOT ShadowOS beta : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_alpha/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_alpha/ quiet splash nouveau.modeset=0
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/intel_ucode.img
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/amd_ucode.img
  initrd http://91.121.38.1:8080/live_alpha/arch/boot/x86_64/archiso.img
  boot

:test_hardware
  echo Starting hardware test
  goto shell

:test_memtest
  echo Starting memory test
  sanboot http://91.121.38.1:8080/live/Memtest86-7.5.iso || goto failed

:test_gputest
  echo Starting gpu test
  goto shell

:reboot
  reboot

:shutdown
  poweroff

:failed
  echo Booting failed, dropping to shell and freeing memory.
  imgfree
  goto shell

:shell
  echo Type 'poweroff' to shutdown the device.
  echo Type 'reboot' to reboot the device.
  echo Type 'help' for more IPXE commands.
  echo .......................................
  route #IP INFO
  echo
  shell
