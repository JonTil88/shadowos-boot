# Live OS install

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Known Issues](#known-issues)
- [Flashing the USB flash Drive](#flashing-usb-flash-drive)
- [HTTP live boot](#http-boot)
- [Discord servers](#discord-servers)
- [Support](#support)
- [Maintainers](#maintainers)
- [Disclaimer](#disclaimer)

<!-- /TOC -->

## Introduction

This project will network boot in RAM you into a fully fonctionnal **Arch Linux** environment with Shadow, at first it was aimed to give a second life to old hardware with broken hard drive but quickly it was also nice for many others use-cases. (Debug, Easy Check for Linux compliance, Work PC, Easy switch from Windows to Linux...)

Booting into this Live OS to use Shadow requires no Linux knowledge at all.

**So far we handle:**
 - UK/US/DE/DK/FR (+Apple Variant) Keyboard
 - USB Mouse + Touch/TrackPads
 - Sound Output
 - Sound Input (Microphones)
 - Gamepads (wired + bluetooth)
 - Network configuration
 - Laptop battery indicator

## Requirements

- **4096MB+** of RAM x86_64 PC with an **ethernet RJ45 cable** and served by **DHCP server** (Shadow Stream won't start on a Virtual Machine, it requires a GPU with hardware decoding feature)
- **Compatible GPU** Intel/AMD/"Nvidia(**> Geforce 8** & **< GTX 750**), it should work automaticaly if you have an **Intel iGPU available**" with dedicated hardware H.26x decoder, and will only work in H.264.
- Fast connection or enough time to download approximatively **1GB** (If you use an USB/Ethernet adapter, please plug it on an USB 2 port if it's not working)
- Enough knowledge to flash an USB flash drive and boot on it from BIOS
- The **Secure Boot** option must be disabled in the bios to be able to boot on the USB drive on **EFI bios**

## Known Issues

- AZERTY keyboard layout by default (you can change that with a right click on the desktop, open an issue if you want additional keyboard layout)
- Some network card can't be find in EFI boot mode. Try to change your boot configuration to legacy to fix the problem
- Some Nvidia GPU hangs during boot (use at boot "Nouveau disabled" entries)
- **NVIDIA GPU**: > Geforce 8 & < GTX 750 is still a work in progress, very old one and newer are unsupported by liveOS atm (check [other projects](https://gitlab.com/aar642/libva-vdpau-driver) for details)
- Shadow Credentials are **not saved** (it is planned to be saved on USB Flash Drive at some point)
- Shadow Authentication code **is requested after each reboot**
- AMD GPU can get black screen (Try in H265, it may workaround the issue)

> *If you find an issue, feel free to report it here: https://gitlab.com/aar642/shadowos-boot/issues*

## Flashing the USB flash Drive

### Requirements

- 16MB+ USB Flash Drive
- Download either  [EFI](https://gitlab.com/aar642/shadowos-boot/-/jobs/artifacts/master/raw/ShadowBoot_EFI_master.img?job=ipxe_build) or [Legacy](https://gitlab.com/aar642/shadowos-boot/-/jobs/artifacts/master/raw/ShadowBoot_LEGACY_master.img?job=ipxe_build) image
- [Etcher](https://www.balena.io/etcher/) installed - Available for Windows / Mac / Linux
- **This steps will wipe all the datas on the USB key**

## USB Live Boot

![Etcher](Docs/Etcher.png "Etcher")

- Select the the ShadowOS image
- Select the USB flash Drive you want to flash ShadowOS
- Click *Flash*

### Boot Shadow Live
- Change Bios to boot on USB Flash Drive
- Wait for timer or select "Boot from network stable version of ShadowOS" and press enter
- Wait until Shadow Live is fully booted
- **Connect to your Shadow  !**

## HTTP Live boot

### Requirements

 - An UEFI Bios with HTTP Boot option

### Boot Shadow Live

 - Enter URL in Bios HTTP Boot option : http://91.121.38.1:8080/shadow.efi
 - Boot from HTTP Boot option
 - **Connect to your Shadow !**

## Boot Menu

![Boot Menu](Docs/boot_menu.png "Boot Menu")

![ShadowOS](Docs/shadow_os.png "Shadow OS")

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Support

| Success on             | Failed on            |
| :--------------------: | :------------------: |
| Nvidia GTX 760         | Nvidia 750 TI        |
| Nvidia GT 720M         | Nvidia 970M*         |
| Nvidia GTX 680         | Nvidia 970*          |    
| Nvidia 550 TI          | Nvidia GT 1030*      |
| AMD Radeon 520         | Nvidia GTX 1050*     |
| AMD Radeon RX 560      | Nvidia GTX 1060*     |
| AMD Radeon RX 580      | Nvidia GTX 1080*     |
| Intel UHD 600          | Nvidia GTX 950M*     |
| Intel UHD 605          | Intel HD2000-4600    |
| Intel HD Graphics 520  | Intel GMA**          |
| Intel HD Graphics 620  | Nvidia 8600M         |
| Intel HD Graphics 630  | Nvidia GTX 560M*     |
| AMD Radeon HD5870      | Nvidia GT 630M*      |
|                        | Nvidia GTX 660*      |
|                        | Nvidia GTX 670*      |
|                        | Nvidia GTX 950*      |
|                        | Nvidia GTX 960(M)*   |
|                        | Nvidia GT 750M*      |
|                        | Nvidia GTX 760*      |
|                        | Nvidia GTX 765M*     |
|                        | Nvidia GTX 780*      |
|                        | Nvidia RTX 2060*     |
|                        | Nvidia 820M*         |
|                        | Nvidia Quadro K2200* |
|                        | Nvidia Quadro K2100M*|
|                        | Nvidia Quadro K1100M*|
|                        | Intel HD 3000**      |
|                        | Intel HD 4000**      |
|                        | Intel HD 4200**      |
|                        | Intel HD 4400**      |
|                        | Intel HD 4600**      |
|                        | Intel GMA 4500MHD*3  |

> *Nvidia Series 10/20 (& some older) may work with : https://gitlab.com/aar642/libva-vdpau-driver

> **Intel HD will now work on Alpha build of Shadow (HD3000+), if not, open an issue with full content of `vainfo` command.

> *3Intel GMA can be used using GMA branch of LiveOS.

> A special thanks for those who took some time to trial their GPU !

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![GiantPandaRoux#0777](https://cdn.discordapp.com/avatars/267044035032514561/e98147b99f4821c4e806e97fda05e69a.png?size=64 "GiantPandaRoux#0777")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
